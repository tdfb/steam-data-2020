The data made available is divided into four columns:
1) An unique identifier, named AppID, that indexes video games in the data base;
2) A column with datetime objects, named Date, that refers to a game's release date (YYYY-MM-DD format);
3) A column with list objects, named Tags. Each list contains a game's categories, ordered by number of user votes received (e.g., the first entry in the list is the most voted category etc.);
4) A column with list objects, named PlayerTimeSeries. Each list contains the monthly average of players per hour in a game (e.g., if the entry is 200.0, that means that, on average, there were 200 users playing that game every hour of the month). The list is sorted from a game's launch (cf. column 2), i.e., the first entry is the release month of a game etc.

Regarding data curation, the criteria of data curation were the following:

a) Initially, we constructed the list of games available in Steam from the steamdb.info website (https://steamdb.info/). This list only contained the unique identifier, the aforementioned application ID (AppID).

b) We then compared the total of games in this list with the number of games available in Steam (https://store.steampowered.com/search?category1=998) at the time of data collection (Jan. 2021). These two number coincided (53,714).

c) Using the AppID, we crawled the Steamcharts website (https://steamcharts.com/) to obtain the time series of each game. Not all games indexed in Steam had an entry in Steamcharts (upcoming releases, games in Early Access etc.). The resulting list contained 21,888 entries.

d) We excluded games released in the first six months of 2012 since steamcharts started data collection in July, 2012 (https://steamcharts.com/about). The final list contained 21,752 games.

e) Finally, with this last list's application IDs, we queried the Steam API to extract metadata from these games (release date and tags). This allowed us to build the dataframe in this project.


**Guide to re-obtain the results presented in the article**

i) Starting from the CSV file, aggregate and tally the games by release month (the 'Date' column), obtaining Fig. 1B;

ii) From this aggregation and the game's ID (the 'AppID' column), aggregate the time series (the 'PlayerTimeSeries' column) to obtain Fig. 1A;

iii) From the release month, aggregate the games into trimesters and aggregate the time series. Then, calculate means and standard deviations to obtain Fig. 2A and B. Finally, calculate the log of the data and the respective means and standard deviations to obtain Fig. 2C and D;

iv) Choose a random trimester from the previous ensembles. Calculate the probability distribution using a lognormal transformation to obtain Fig. 3A. Estimate the optimal Box-Cox parameter and transform the data to obtain Fig. 3B. Calculate the optimal Box-Cox parameter (with 0.95 confidence interval) for each successive quarter to obtain Fig. 3C;

v) Proceed similarly with the global data to obtain Fig. 4;

vi) Proceed similarly to Fig. 1 with the Indie category data to obtain Fig. 5;

vii) To obtain Fig. 6, a combination of the previous techniques is necessary: estimate the optimal Box-Cox parameter for the corresponding category and calculate the probability distribution, thus arriving at Fig. 6A. Estimate the optimal parameter at each three-monthly step to obtain the time evolution (Fig. 6B). Fig. 6C is obtained from the first step, considering only the largest categories;

viii) Tally the number of categories considering the release months of each game to arrive at Fig. 7A. Tally the number of games in each category, estimate the optimal Box-Cox parameter for this data, and calculate a probability distribution (Fig. 7B). Proceed analogously but biyearly to obtain Fig. 7C.
